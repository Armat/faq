<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Faq3
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Main helper FAQ
 *
 * @category    Webinse
 * @package     Webinse_Faq3
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
class Webinse_Faq3_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Retrieve all faq sorted by date
     *
     * @return Webinse_Faq3_Model_Resource_Faq_Collection
     */
    public function getAllFaq()
    {
        /**
         * @todo get collection of all faq
         */
        $collection = Mage::getModel('webinse_faq3/faq')->getCollection();
        return $collection;
    }

    /**
     * Retrieve faq by id
     *
     * @return Mage_Core_Model_Abstract|Webinse_Faq3_Model_Faq
     */
    public function getFaqById()
    {
        /**
         * @todo get params from url and load model
         */
        $faqId = Mage::app()->getRequest()->getParam('id');
        $faqObject = Mage::getModel('webinse_faq3/faq')->load($faqId);
        return $faqObject;
    }
}
