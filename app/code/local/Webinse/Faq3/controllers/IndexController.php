<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Faq3
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Frontend index controller
 *
 * @category    Webinse
 * @package     Webinse_Faq3
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
class Webinse_Faq3_IndexController extends Mage_Core_Controller_Front_Action
{

    protected function _initLayout()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * This method is output all questions and answers to them
     * For example you may visit the following URL http://example.com/frontName/index/getAllFaq
     */
    public function getAllFaqAction()
    {
        $this->_initLayout();
    }

    /**
     * Render form to add new faq
     */
    public function addNewFaqAction()
    {
        // the same form is used to create and edit
        $this->_forward('editFaq');
    }

    /**
     * Render form to edit faq
     */
    public function editFaqAction()
    {
        // @todo here you must load and render layout
        $this->_initLayout();
    }

    /**
     * Save faq by using id or add new record
     */
    public function saveAction()
    {
        /**
         * @todo here you must realize create or edit logic
         *      if id exist in post, you can load model by using it otherwise create new record
         *      add message about successful saving or editing by using session (see models such as Mage_Core_Model_Session and extended classes)
         */
        // we get the data sent with POST
        $question = $this->getRequest()->getPost('question');
        $answer = $this->getRequest()->getPost('answer');
        if(!empty($question) && !empty($answer)) {
            if(empty($this->getRequest()->getPost('id')))
            {
                // insert
                $message = 'added';
                $record = Mage::getModel('webinse_faq3/faq');
                $record->setData('question',$question)
                        ->setData('answer',$answer)
                        ->save();
            }
            else {
                // update
                $message = 'updated';
                $id = (integer)$this->getRequest()->getPost('id');
                $record = Mage::getModel('webinse_faq3/faq')->load($id);
                $record->setData('id', $id)
                        ->setData('question',$question)
                        ->setData('answer',$answer)
                        ->save();
            }

            if($record) {
                Mage::getSingleton('core/session')->addSuccess('Success ' . $message);
            }
            else {
                Mage::getSingleton('core/session')->addError('Error when trying ' . $message);
            }
        }
        else {
            Mage::getSingleton('core/session')->addError('Error. Question and answer fields are required');
        }

        $this->_redirect('faq3/index/getAllFaq');
    }

    /**
     * Delete faq by id
     */
    public function deleteAction()
    {
        /**
         * @todo get id sent by url and delete faq
         *       add message by using session
         */
        $id = $this->getRequest()->getParam('id');
        if(!empty($id)) {
            $faqObject = Mage::getModel('webinse_faq3/faq')->load($id);
        }
        try {
            $faqObject->delete();
            Mage::getSingleton('core/session')->addSuccess('Success deleted');
            $this->_redirect('faq3/index/getAllFaq');

        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirect('faq3/index/getAllFaq');
        }
    }

}
