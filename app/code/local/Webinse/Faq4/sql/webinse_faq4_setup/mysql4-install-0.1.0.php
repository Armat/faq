<?php
$installer = $this;
$installer->startSetup();
/**
 * @todo add table webinse_faq4 (don't use method run(), it's deprecated method)
 *
 * entity_id    - int, not null, auto increment, primary key
 * question     - varchar
 * answer       - varchar
 * user_id      - int (current admin ID), set foreign key for this field (related with table - 'admin_user', field - 'user_id')
 */
try {
    $table = $installer->getConnection()->newTable($installer->getTable('webinse_faq4/faq'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
            'identity' => true,
        ), 'Faq ID')
        ->addColumn('question', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'Question field')
        ->addColumn('answer', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => true,
        ), 'Answer field')
        ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => false,
            'nullable' => false,
            'primary' => false,
            'identity' => false,
        ), 'Admin user')
        ->addForeignKey(
            $installer->getFkName('webinse_faq4', 'user_id', 'admin_user','user_id'),
            $installer->getTable('webinse_faq4'),
            $installer->getTable('admin_user'),
            'user_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        )
        ->setComment('Faq 4 traning');
    try {
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    } catch (Zend_Db_Exception $e) {
        echo $e->getMessage();
    }
}
catch (Zend_Db_Exception $e) {
    echo $e->getMessage();
}