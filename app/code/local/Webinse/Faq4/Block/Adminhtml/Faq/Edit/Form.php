<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Faq4
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Adminhtml faq edit form block
 *
 * @category    Webinse
 * @package     Webinse_Faq4
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
class Webinse_Faq4_Block_Adminhtml_Faq_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Prepare form for render
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
    }

    protected function _prepareForm()
    {
        $faq = Mage::registry('current_faq');
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('edit_faq', array(
            'legend' => Mage::helper('webinse_faq4')->__('Faq Details') // @todo: you must set properly URI of your helper
        ));
        /**
         * @todo: add field with faq id
         */
        if ($faq->getId()) {
            $fieldset->addField('entity_id', 'hidden', array(
                'name' => 'entity_id',
                'required' => false,
                'value' => $faq->getId()
            ));
        }
        /**
         * @todo: add other fields, use class for validation (js/prototype/validation.js)
         */
        $fieldset->addField('question', 'textarea', array(
            'label' => 'Question',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'question'
        ));
        $fieldset->addField('answer', 'textarea', array(
            'label' => 'Answer',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'answer'
        ));
        /**
         * @todo: set action to form and other params to form
         */
        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/save'));
        $form->setValues($faq->getData());

        $this->setForm($form);
    }

}
