<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Faq4
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Adminhtml faq grid block
 *
 * @category    Webinse
 * @package     Webinse_Faq4
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
class Webinse_Faq4_Block_Adminhtml_Faq_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _construct()
    {
        $this->setId('faqGrid');
        $this->_controller = 'adminhtml_faq';
        $this->setUseAjax(true);

        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('desc');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('webinse_faq4/faq')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'        => Mage::helper('webinse_faq4')->__('ID'),
            'align'         => 'right',
            'width'         => '20px',
            'filter_index'  => 'entity_id',
            'index'         => 'entity_id'
        ));

        $this->addColumn('user_id', array(
            'header'        => Mage::helper('webinse_faq4')->__('Admin user'),
            'align'         => 'left',
            'width'         => '20px',
            'filter_index'  => 'user_id',
            'index'         => 'user_id',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('question', array(
            'header'        => Mage::helper('webinse_faq4')->__('Question'),
            'align'         => 'left',
            'filter_index'  => 'question',
            'index'         => 'question',
            'type'          => 'text',
            'truncate'      => 50,
            'escape'        => true,
        ));

        $this->addColumn('action', array(
            'header'    => Mage::helper('webinse_faq4')->__('Action'),
            'width'     => '140px',
            'type'      => 'action',
            'getter'     => 'getId',
            'actions'   => array(
                array(
                    'caption' => Mage::helper('webinse_faq4')->__('Edit'),
                    'url'     => array(
                        'base'=>'*/*/edit',
                    ),
                    'field'   => 'id'
                ),
                array(
                    'caption' => Mage::helper('webinse_faq4')->__('Delete'),
                    'url'     => array(
                        'base'=>'*/*/delete',
                    ),
                    'field'   => 'id'
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'id',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($faq)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $faq->getId(),
        ));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}
